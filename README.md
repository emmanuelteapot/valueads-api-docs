# valueads-api-docs

Base url
`https://api.valueads.biz`

Login
`POST /login`
``` javascript
{
  "email": "string",
  "password": "string",
}
```

Response
``` javascript
{"token" : "string"}
```

Create a client
`POST /users/clients`
``` javascript
{
  "clientName": "string",
  "clientLogo": "string",
  "receiptTemplate": "string"
}
```

Response 
```javascript
{
  "clientName": "string",
  "clientUniqueId": "string",
}
```

Update a client
`PATCH /users/clients`
``` javascript
{
  "clientName": "string", // optional
  "clientLogo": "string", // optional
  "receiptTemplate": "string" // optional
}
```

Response 
```javascript
 204
```


Get a list of clients
`GET /users/clients`

response
``` javascript
[
    {
    "clientName": "string",
    "clientUniqueId": "string",
    }
]
```

Create receipt
`POST /clients/{id}/receipts`
``` javascript
{
  "amount": "string",
  "description": "string",
  "receiptNumber": "string"
}
```
Response, short link eg: 
``` javascript
// v-ad.tk/HRtY
```



